
var IPFS = require('ipfs-http-client')
const ipfs = new IPFS({ host: 'ipfs.infura.io', port: 5001, protocol: 'https' })
async function upload(){
  const res = await ipfs.addFromFs('dist', {recursive: true})
  console.log(res)
}
upload()
