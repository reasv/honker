import ipfsNode from 'ipfs'
const Buffer = require('buffer').Buffer
var ipfs = null

export async function init(repo = 'ipfs-repo', pass = 'c991a3677d55c6e98a33782770d6a364') { // dummy password
  ipfs = new ipfsNode(
    { repo: repo,
      pass: pass,
      EXPERIMENTAL: { pubsub: true, ipnsPubsub: true },
      config: {
        Addresses: {
          Swarm: [
            '/dns4/ws-star.discovery.libp2p.io/tcp/443/wss/p2p-websocket-star'
          ]
        }
      }
    })
  return new Promise(function(resolve, reject) {
    ipfs.on('ready', () => {
      resolve(ipfs)
    })
  })
}
export async function createUser(name) {
  const key_id = await genKey()
  const user_id = key_id
  console.log(user_id)
  await ipfs.files.mkdir(`/${user_id}/honks`, {parents: true})
  console.log("Created directory")
  var profile = {
    name: name
  }
  await ipfs.files.write(`/${user_id}/profile.json`, makeBuffer(profile), {create: true})
  console.log("Created profile")
  const record = await updateRecord(user_id, false)
  console.log(record)
  return record
}
export async function genKey() {
  const temp_key_name = Date.now().toString()
  const key = await ipfs.key.gen(temp_key_name, {
    type: 'rsa',
    size: 2048
  })
  await ipfs.key.rename(temp_key_name, key.id)
  return key.id
}
export async function keyExists(name){
  const key_list = await ipfs.key.list()
  const matching = (key_list.filter(k => k.name == name))
  if (matching.length > 0) {
    return true
  } else {
    return false
  }
}
export function makeBuffer(obj){
  return Buffer.from(JSON.stringify(obj))
}
export function makeObject(buff){
  return JSON.parse(buff.toString("utf-8"))
}
export async function publishRecord(user_id, hash){
  const key_name = user_id
  const record = await ipfs.name.publish(hash, {key: key_name})
  return record
}
export async function updateRecord(user_id, asynchronous=true){
  await ipfs.files.flush(`/${user_id}`)
  const folder_hash = (await ipfs.files.stat(`/${user_id}`, { hash: true })).hash
  let record
  if (asynchronous){
    publishRecord(user_id, folder_hash)
    record = { name: user_id, value: "/ipfs/"+folder_hash}
  } else {
    record = await publishRecord(user_id, folder_hash)
  }
  return record
}
export async function makeHonk(user_id, honk) {
  await ipfs.files.write(`/${user_id}/honks/${honk.time}`, makeBuffer(honk), { parents: true, create: true })
  const record = await updateRecord(user_id)
  return record
}
export async function removeHonk(user_id, honk_time) {
  try {
    await ipfs.files.rm(`/${user_id}/honks/${honk_time}`)
    const record = updateRecord(user_id)
    return { record: record, success: true }
  } catch (e){
    const record = updateRecord(user_id)
    return { record: record, success: false }
  }
}
export async function getHonks(user_id, amount = 0, offset = 0) {
  const files = await ipfs.files.ls(`/${user_id}/honks`, {sort: true})
  var file_slice
  if (amount == 0) {
    file_slice = files.reverse()
  } else {
    file_slice = files.reverse().slice(offset, offset + amount)
  }
  var honks = []
  for (var i in file_slice){
    var filename = `/${user_id}/honks/${file_slice[i].name}`
    var file = await ipfs.files.read(filename)
    honks.push(makeObject(file))
  }
  return honks
}
export async function getProfile(user_id){
  const profile_file = await ipfs.files.read(`/${user_id}/profile.json`)
  return makeObject(profile_file)
}
export async function getRemoteUser(id, is_cid = false) {
  let cid
  let user_id
  if (!is_cid){
    user_id = id
    const res = await ipfs.name.resolve(user_id)
    cid = res.path
  } else {
    cid = id
  }
  var user = {
    profile: {},
    honks: [],
    value: cid
  }
  user.profile = makeObject(await ipfs.cat(`${cid}/profile.json`))
  //console.log(await ipfs.ls(`${cid}/honks/`))
  const files = await ipfs.get(`${cid}/honks/`)
  files.forEach((file) => {
    if (file.content) {
      user.honks.push(makeObject(file.content))
    }
  })
  user.honks.sort((a,b) => b.time - a.time ) // reverse chronological order
  console.log(user)
  return user
}
