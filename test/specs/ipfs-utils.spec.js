import { mount } from '@vue/test-utils'
import test from 'ava'
import {init, createUser, genKey, keyExists, makeHonk, removeHonk, getHonks, getRemoteUser, makeBuffer} from '../../util/ipfs-utils.js'

test.serial('Pubsub', async(t) =>{
  const ipfs = await init('repo/'+Date.now())
  const ipfs_1 = await init('repo/1'+Date.now())
  const topic = 'Topic'+Date.now()

  await ipfs.pubsub.subscribe(topic, (msg) => console.log(msg))
  console.log(`IPFS_0: subscribed to ${topic}`)
  await ipfs_1.pubsub.subscribe(topic, (msg) => console.log(msg))
  console.log(`IPFS_1: subscribed to ${topic}`)
  await ipfs.pubsub.publish(topic, makeBuffer({time: Date.now()}))
  var peers_0 = await ipfs.pubsub.peers(topic)
  var peers_1 = await ipfs_1.pubsub.peers(topic)
  console.log(peers_0, peers_1)
  t.is(peers_0.length > 0, true, "Peers are connected to 0")
  t.is(peers_1.length > 0, true, "Peers are connected to 1")
  t.pass()
})
test.serial('IPNS', async(t) => {
  const ipfs = await init('repo/'+Date.now())
  const ipfs_1 = await init('repo/1'+Date.now())

  const cid = "/ipfs/QmYwAPJzv5CZsnA625s3Xf2nemtYgPpHdWEz79ojWnPbdG"
  const record = await ipfs.name.publish(cid)

  const res_0 = await ipfs.name.resolve(record.name)
  const res_1 = await ipfs_1.name.resolve(record.name)

  t.is(res_0.path, cid, "Name resolves on same node")
  t.is(res_1.path, cid, "Name resolves on other node")
  t.pass()
})

test.serial('Generating Keys', async (t) => {
  const ipfs = await init('repo/'+Date.now())
  const key = await genKey()
  t.is(await keyExists(key), true, "Key was created successfully")
  ipfs.stop(() => {
    t.pass()
  })
})

test.serial('Create User', async (t) => {
  const ipfs = await init('repo/'+Date.now())
  const user_name = "TestUser"+Date.now()
  const record = await createUser(user_name)
  const res_record = await ipfs.name.resolve(record.name)
  t.is(res_record.path, record.value, "User record resolves correctly")
  const profile_file = await ipfs.cat(`${record.value}/profile.json`)
  const profile = JSON.parse(profile_file.toString("utf-8"))
  t.is(profile.name, user_name, "User profile created correctly")
  ipfs.stop(()=>{
    t.pass()
  })
})

test.serial('User Honks', async (t) => {
  const ipfs = await init('repo/'+Date.now())
  const user_name = "TestUser"+Date.now()
  var record = await createUser(user_name)
  const user_id = record.name
  const honk0Text = "TestText0"+Date.now()
  const honk0 = {
    time: Date.now(),
    text: honk0Text
  }
  record = await makeHonk(user_id, honk0)
  const honkText = "TestText"+Date.now()
  const honk = {
    time: Date.now(),
    text: honkText
  }
  console.log(user_id)
  record = await makeHonk(user_id, honk)
  var honks = await getHonks(user_id, 2, 0)
  t.is(honks[0].text, honk.text, "Honk created and retrieved correctly")
  record = await removeHonk(user_id, honk.time)
  honks = await getHonks(user_id, 2, 0)
  t.is(honks[0].text, honk0Text,  "Honk removed")
  const user_data = await getRemoteUser(user_id)
  t.is(user_data.honks[0].text, honk0Text, "Remote user fetched")
  ipfs.stop(() => {
    t.pass()
  })
})

